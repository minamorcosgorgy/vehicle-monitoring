import { Customer } from './customer.model';
export class Vehicle {
    id: string;
    regNr: string;
    state: string;
    customer: Customer;

}
