import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireDatabase } from '@angular/fire/database';
import { Vehicle } from 'src/app/models/vehicle.model';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class VehicleService {

  constructor(private _http: HttpClient,
    private firebaseService: AngularFirestore,
    private db: AngularFireDatabase) { }  

   getVehicles() {
    return this.db.list('/vehicles').snapshotChanges();
}
}
