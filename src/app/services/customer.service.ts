import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Customer } from '../models/customer.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable()
export class CustomerService {
  private customerList;
  constructor(private _http: HttpClient,
    private firebaseService: AngularFirestore,
    private db: AngularFireDatabase) {
      this.getCustomerList().subscribe( data => 
        {
          this.customerList = data
        }
      );
     }
  

   // getting customer by id 
  getCustomer(id) {
    return this.db.object(`/customers/${id}`).valueChanges();
  }
  // getting customers list
  getCustomerList() {
    return this.db.object(`/customers/`).valueChanges();
  }
}
