import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesListComponent } from './vehicles-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { BackendSimulatorModule } from '../backend-simulator/backend-simulator.module';
import { LayoutModule, HttpLoaderFactory } from '../layout/layout.module';
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { BackendSimulatorService } from '../backend-simulator/backend-simulator.service';
import { CustomerService } from '../services/customer.service';
import { MatTableDataSource } from '@angular/material';


describe('VehiclesListComponent', () => {
  let component: VehiclesListComponent;
  let fixture: ComponentFixture<VehiclesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialModule,
        LayoutModule,
        BackendSimulatorModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        HttpClientModule,
           TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [
        VehiclesListComponent
      ],
      providers: [AngularFirestore, BackendSimulatorService, CustomerService],
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('vehicles should be empty', () => {
    expect(component.vehicles).toBeUndefined();
  });

  it('filter vehicles by customer', () =>{
    component.vehicles = [ {
      "customer" : {
        "address": "Norra Backebo 45",
        "id": "uy785g5",
        "name": "Hafsah Mahoney"
      },
      "id" : "sadaas853",
      "regNr" : "IKO858",
      "state" : "Available"
    }, {
      "customer" : {
        "address": "Overhogdal 29",
        "id": "6r56ef75e",
        "name": "Regina Robbins"
      },
      "id" : "sdasaoe5",
      "regNr" : "GHG888",
      "state" : "Busy"
    }, {
      "customer" : {
        "address": "Torggatan 30",
        "id": "76g565875",
        "name": "Tim Hess"
      },
      "id" : "asdaaw8",
      "regNr" : "BVC666",
      "state" : "Available"
    }, {
      "customer" : {
        "address": "LemesjO 84",
        "id": "6ryg765r76",
        "name": "Benjamin Kidd"
      },
      "id" : "sadss8as",
      "regNr" : "LKI985",
      "state" : "Available"
    }, {
      "customer" : {
        "address": "LemesjO 84",
        "id": "6ryg765r76",
        "name": "Benjamin Kidd"
      },
      "id" : "sadawds5",
      "regNr" : "GGF565",
      "state" : "Available"
    } ];
    
    // check list is not empty
    expect(component.vehicles.length).toEqual(5);
    
    // initialize datasource
    component.initDatasource()
    fixture.detectChanges();

    // apply filter to the data source
    component.applyFilter('Hafsah');

    // check filter value
    expect(component.dataSource.filter).toBe('hafsah');

    // check datasource is filtered
    expect(component.dataSource.filteredData.length).toBe(1);
  });

  it('filter vehicles by status', () =>{
    component.vehicles = [ {
      "customer" : {
        "address": "Norra Backebo 45",
        "id": "uy785g5",
        "name": "Hafsah Mahoney"
      },
      "id" : "sadaas853",
      "regNr" : "IKO858",
      "state" : "Available"
    }, {
      "customer" : {
        "address": "Overhogdal 29",
        "id": "6r56ef75e",
        "name": "Regina Robbins"
      },
      "id" : "sdasaoe5",
      "regNr" : "GHG888",
      "state" : "Busy"
    }, {
      "customer" : {
        "address": "Torggatan 30",
        "id": "76g565875",
        "name": "Tim Hess"
      },
      "id" : "asdaaw8",
      "regNr" : "BVC666",
      "state" : "Available"
    }, {
      "customer" : {
        "address": "LemesjO 84",
        "id": "6ryg765r76",
        "name": "Benjamin Kidd"
      },
      "id" : "sadss8as",
      "regNr" : "LKI985",
      "state" : "Available"
    }, {
      "customer" : {
        "address": "LemesjO 84",
        "id": "6ryg765r76",
        "name": "Benjamin Kidd"
      },
      "id" : "sadawds5",
      "regNr" : "GGF565",
      "state" : "Busy"
    } ];
    
    // check list is not empty
    expect(component.vehicles.length).toEqual(5);
    
    // initialize datasource
    component.initDatasource()
    fixture.detectChanges();

    // apply filter to the data source
    component.applyFilter('Busy');

    // check filter value
    expect(component.dataSource.filter).toBe('busy');

    // check datasource is filtered
    expect(component.dataSource.filteredData.length).toBe(2);
  });

});
