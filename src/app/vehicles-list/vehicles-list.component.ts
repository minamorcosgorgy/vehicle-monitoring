import { Component, OnInit, ViewChild } from '@angular/core';
import { VehicleService } from 'src/app/services/vehicle.service';
import { Vehicle } from 'src/app/models/vehicle.model';
import { Customer } from 'src/app/models/customer.model';
import { MatSort, MatTableDataSource, MatPaginator, MatInputModule } from '@angular/material';
import { CustomerService } from '../services/customer.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-vehicles-list',
  templateUrl: './vehicles-list.component.html',
  styleUrls: ['./vehicles-list.component.css']
})
export class VehiclesListComponent implements OnInit {
  displayedColumns = ['id', 'state', 'regNr', 'name', 'address'];
  vehicles: Vehicle[];
  dataSource: MatTableDataSource<any>;
  filterStr = undefined;

  constructor(public translate: TranslateService, private vehicleService: VehicleService, private customerService: CustomerService) {


    // getting the vehicles data 
    this.vehicleService.getVehicles().subscribe(data => {
      if (!this.vehicles) {
        this.vehicles = data.map(e => { return e.payload.toJSON() as Vehicle });
        this.vehicles.forEach(vehicle => {
          this.customerService.getCustomer(vehicle.customer).subscribe(customer =>
            vehicle.customer = customer as Customer
          );
        });
        this.initDatasource();
      } else {
        for (let index = 0; index < data.length; index++) {
          this.vehicles[index].state = (data[index].payload.toJSON() as Vehicle).state;
        }
      }
      if (this.filterStr) {
        this.applyFilter(this.filterStr);
      }
    });

  }

  applyFilter(filterValue: string) {
    this.filterStr = filterValue;
    this.dataSource.filter = this.filterStr.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }



  // initDatasourse handles the first insitation of data source and handling the nested object for angular material filter
  initDatasource(){
    this.dataSource = new MatTableDataSource(this.vehicles);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.dataSource.filterPredicate = (data, filter: string) => {
      const accumulator = (currentTerm, key) => {
        return key === 'customer' ? currentTerm + data.customer.name : currentTerm + data[key];
      };
      const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
      // Transform the filter by converting it to lowercase and removing whitespace.
      const transformedFilter = filter.trim().toLowerCase();
      return dataStr.indexOf(transformedFilter) !== -1;
    };
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() { }

  ngAfterViewInit(): void {

  }

}