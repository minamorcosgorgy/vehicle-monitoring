import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  fullscreen;
  constructor(public translate: TranslateService) {
  }
  // language switcher function
  changeLang(lang: string) {
    this.translate.use(lang);
  }

  ngOnInit() {
  }


  // toggling fucll screen function
  toggleFullScreen() {
    if (this.fullscreen) {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document['mozCancelFullScreen']) {
        /* Firefox */
        document['mozCancelFullScreen']();
      } else if (document['webkitExitFullscreen']) {
        /* Chrome, Safari and Opera */
        document['webkitExitFullscreen']();
      } else if (document['msExitFullscreen']) {
        /* IE/Edge */
        document['msExitFullscreen']();
      }
    } else {
      let elem = document.documentElement;
      let methodToBeInvoked = elem.requestFullscreen ||
        elem['webkitRequestFullScreen'] || elem['mozRequestFullscreen']
        ||
        elem['msRequestFullscreen'];
      if (methodToBeInvoked) methodToBeInvoked.call(elem);
    }
    this.fullscreen = !this.fullscreen;
  }


}
