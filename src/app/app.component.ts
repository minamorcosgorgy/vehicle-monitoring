import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { BackendSimulatorService } from './backend-simulator/backend-simulator.service';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'vehicle-monitoring';
  constructor(public translate: TranslateService, private backendSimulator: BackendSimulatorService) {
    translate.setDefaultLang('en');
  this.backendSimulator.startBackendSimulator()
  }
}
