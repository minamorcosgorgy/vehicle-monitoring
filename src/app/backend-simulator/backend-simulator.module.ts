import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackendSimulatorComponent } from './backend-simulator/backend-simulator.component';
import { BackendSimulatorService } from './backend-simulator.service';

@NgModule({
  declarations: [BackendSimulatorComponent],
  imports: [
    CommonModule
  ],
  providers: [
  ]
})
export class BackendSimulatorModule { }
