import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BackendSimulatorService {
  VEHICLES_PING_INTERVAL = 6000;
  VEHICLES_NO = 23;

  constructor(private _http: HttpClient,
    private db: AngularFireDatabase) { 
  }


  // update the reat time firebase data base
    updateVehicleState(id, state): Promise<any> {
      return this.db.database.ref(`/vehicles/${id}`).update({state: state});
    }
  // choose a random vehicle to update its state
    updateVehiclesStatesRandomly(vehiclesNo) {
      for (let index = 0; index < vehiclesNo; index++) {
        let state = Math.round(Math.random());
        this.updateVehicleState(index, state === 1 ? 'Available':'Busy');
      }
    }

  // start the backend simulator by givving the function the time and the nuber of vehicles  
  startBackendSimulator(){
    setInterval( () =>
    this.updateVehiclesStatesRandomly(this.VEHICLES_NO), 
    this.VEHICLES_PING_INTERVAL
  );
}
}
