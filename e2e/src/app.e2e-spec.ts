import { AppPage } from './app.po';

import { browser, logging, element, by, protractor, $$, $ } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  // it('should display welcome message', () => {
  //   page.navigateTo();
  //   expect(page.getTitleText()).toEqual('Welcome to vehicle-monitoring!');
  // });
  it('should translate to swedish', () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    let languageBtn = element(by.css('.lang-btn'));
    languageBtn.click();
    browser.sleep(1000);
    let swBtn = element(by.css('.lang-switch-sv'));
    swBtn.click();
    browser.sleep(1000);
    expect(element(by.css('.welcome')).getText()).toContain('Välkommen till Fordonsövervakning');
  });

  it('should do filter', () => {
    page.navigateTo().then( ()=> {
      setTimeout(() => {
        const oldPagingInfo = page.getPagingInfo();
        page.setFilterText('emma');
        const newPagingInfo = page.getPagingInfo();
         expect(oldPagingInfo).not.toEqual(newPagingInfo); 
      }, 20000);

    });
  });
 
  it('should open side bar ', () => {
    page.navigateTo();
    browser.ignoreSynchronization = true;
    let menuBtn = element(by.css('.side-btn'));
    menuBtn.click();
    browser.sleep(1000);
    expect(element(by.css('.home-list')).getText()).toContain('Home');
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
