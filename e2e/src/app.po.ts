import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root h1')).getText() as Promise<string>;
  }

  setFilterText(filtertext) {
     element(by.css('input')).sendKeys(filtertext);
  }

  getPagingInfo() {
    return element(by.css('.mat-paginator-range-label')).getText() as Promise<string>;
  }

  getRowsNo() {
    console.log('getRowsNo');
    return this.getPagingInfo().then((pagingInfo) => {return pagingInfo.split(' ')[4]}); 
  }
}
